package com.curso.streams;

public class Ingrediente {

	private int id;
	private String nombre, tipo_sabor;

	public Ingrediente(int id, String nombre, String tipo_sabor) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.tipo_sabor = tipo_sabor;
	}

	public Ingrediente() {
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getTipo_sabor() {
		return tipo_sabor;
	}

	public void setTipo_sabor(String tipo_sabor) {
		this.tipo_sabor = tipo_sabor;
	}

	@Override
	public String toString() {
		return "Ingrediente [id=" + id + ", nombre=" + nombre + ", tipo_sabor=" + tipo_sabor + "]";
	}

}
