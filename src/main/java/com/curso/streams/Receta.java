package com.curso.streams;

import java.util.List;

public class Receta {

	private String nombre, origen;
	private Integer id, precio;
	private List<Ingrediente> ingredientes;

	public Receta() {
	}

	public Receta(String nombre, String origen, Integer id, Integer precio) {
		super();
		this.nombre = nombre;
		this.origen = origen;
		this.id = id;
		this.precio = precio;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getOrigen() {
		return origen;
	}

	public void setOrigen(String origen) {
		this.origen = origen;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getPrecio() {
		return precio;
	}

	public void setPrecio(Integer precio) {
		this.precio = precio;
	}

	public List<Ingrediente> getIngredientes() {
		return ingredientes;
	}

	public void setIngredientes(List<Ingrediente> ingredientes) {
		this.ingredientes = ingredientes;
	}

	@Override
	public String toString() {
		return "Receta [nombre=" + nombre + ", origen=" + origen + ", id=" + id + ", precio=" + precio
				+ ", ingredientes=" + ingredientes + "]";
	}

}
