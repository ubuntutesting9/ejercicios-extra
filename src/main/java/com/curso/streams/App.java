package com.curso.streams;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

public class App {

	public static void main(String[] args) {

		Ingrediente ingrediente1 = new Ingrediente(1, "patata", "neutro");
		Ingrediente ingrediente2 = new Ingrediente(2, "tomate", "dulce");
		Ingrediente ingrediente3 = new Ingrediente(3, "pimiento", "amargo");
		Ingrediente ingrediente4 = new Ingrediente(4, "cebolla", "picante");
		Ingrediente ingrediente5 = new Ingrediente(5, "huevo", "neutro");
		
		Receta receta1 = new Receta("tortilla de patatas", "España", 1, 3);
		Receta receta2 = new Receta("pimientos asados", "España", 2, 2);
		Receta receta3 = new Receta("patata asada", "Perú", 3, 5);
		
		List<Ingrediente> ingredientes_tortilla = new ArrayList<>();
		ingredientes_tortilla.add(ingrediente1);
		ingredientes_tortilla.add(ingrediente4);
		ingredientes_tortilla.add(ingrediente5);
		
		List<Ingrediente> ingredientes_pimientos_asados = new ArrayList<>();
		ingredientes_pimientos_asados.add(ingrediente2);
		ingredientes_pimientos_asados.add(ingrediente4);
		ingredientes_pimientos_asados.add(ingrediente3);
		
		List<Ingrediente> ingredientes_patata_asada = new ArrayList<>();
		ingredientes_patata_asada.add(ingrediente1);
		ingredientes_patata_asada.add(ingrediente2);
		ingredientes_patata_asada.add(ingrediente4);
		
		
		receta1.setIngredientes(ingredientes_tortilla);
		receta2.setIngredientes(ingredientes_pimientos_asados);
		receta3.setIngredientes(ingredientes_patata_asada);
		
		//App.showIngredients(receta3);
		App.checkRecipeContainsPotateoAndHasNeutralFlavour(receta2);
		
	}
	
	// Stream que recorre todos los ingredientes de una receta
	public static void showIngredients(Receta receta) {
		Stream<Ingrediente> ingredientsStream = receta.getIngredientes().stream();
		ingredientsStream.forEach(ingredient -> System.out.println(ingredient));
		
	}
	
	// Stream que filtra si la receta tiene ingredientes neutros y si contiene patata
	public static void checkRecipeContainsPotateoAndHasNeutralFlavour(Receta receta) {
		Stream<Ingrediente> ingredientsStream = receta.getIngredientes().stream();
		ingredientsStream.filter(ingrediente -> ingrediente.getTipo_sabor()=="neutro").
		filter(ingrediente -> ingrediente.getNombre()=="patata")
		;
	}
	
	
	
}
