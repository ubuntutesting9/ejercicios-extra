package com.curso.lambdas;

public interface InterfaceBiFunction {

	public abstract Double getProduct(Double firstNumber, Double secondNumber);
	
}
