package com.curso.lambdas;

public interface InterfaceBiPredicate {

	public abstract boolean num1BiggetThanNum2(int num1, int num2);
	
}
