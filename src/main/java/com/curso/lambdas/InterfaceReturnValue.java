package com.curso.lambdas;

public interface InterfaceReturnValue {

	public abstract String returnMessage();
	
}
