package com.curso.lambdas;

import java.util.concurrent.ThreadLocalRandom;
import java.util.function.BiConsumer;
import java.util.function.BiPredicate;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;

public class App {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//supplierLambdaDefiningInterface();
		//App.supplierLambdaUsingExistentInterface();
		//App.consumerLambdaDefiningInterface(35);
		//App.biConsumerLambdaDefiningInterface(22, "Galileo");
		//App.biConsumerLambdaUsingExistentInterface(245678, 987654321);
		//App.functionLambdaDefiningInterface(223);
		//App.functionLambdaUsingExistentInterface("Xavier Román");
		//App.biFunctionLambdaDefiningInterface(3.5, 12.9);
		//App.predicateLambdaDefiningInterface(12);
		//App.predicateLambdaUsingExistentInterface("esternocleidomastoideo");
		//App.biPredicateLambdaDfiningInterface(3, 22);
		//App.biPredicateUsingExistentInterface("Joaquín", "Juan");
	
	}
	
	// Se ha hecho 2 lambdas de cada tipo, 1 donde definíamos nuestra propia interfaz
	// y otra donde se hacía uso de la interfaz predefinida.
	
	// Lambda utilizando un Supplier cuya interfaz definimos nosotros mismos
	public static void supplierLambdaDefiningInterface() {
		InterfaceReturnValue supplierLambda = () -> {return "Esto es un mensaje";};
		
		System.out.println(supplierLambda.returnMessage());
		System.out.println("El tipo de dato devuelto por el método es: "+ supplierLambda.returnMessage().getClass());
	}
	
	// Lambda utilizando un Supplier con la interfaz ya definida de Supplier
	public static void supplierLambdaUsingExistentInterface() {
		Supplier<Double> supplierLambda = () -> {return 25.5;};
		
		System.out.println("El número dado por el lambda es: " + supplierLambda.get());
	}
	
	// Lambda utilizando un Consumer cuya interfaz definimos nosotros mismos
	public static void consumerLambdaDefiningInterface(int numeroEntrada) {
		InterfaceConsumeValue consumerLambda = (numero) -> 
			System.out.println("El numero que has proporcionado es: " + numero);
		
		consumerLambda.consumeValue(numeroEntrada);
	}
	
	// Lambda utilizando la interfaz Consumer
	public static void consumerLambdaUsingExistentInterface(int numeroEntrada) {
		Consumer<Integer> consumerLambda = (numero) -> 
			System.out.println("El numero que has proporcionado es: " + numero);
		
		consumerLambda.accept(numeroEntrada);
	}
	
	// Lambda utilizando BiConsumer con interfaz creada por nosotros mismos
	public static void biConsumerLambdaDefiningInterface(int edadEntrada, String nombreEntrada) {
		InterfaceBiConsumeValue biConsumerLambda = (edad, nombre) -> 
			System.out.println("La persona que has indicado se llama " + nombre
					+ " y tiene " + edad + " años");
		
		biConsumerLambda.consumeTwoValues(edadEntrada, nombreEntrada);
	}
	
	// Lambda utilizando la interfaz de BiConsumer
	public static void biConsumerLambdaUsingExistentInterface(int idCliente, int codigoFactura) {
		BiConsumer<Integer, Integer> biConsumerLambda = (cliente, codFactura) ->
		System.out.println("El cliente con id " + idCliente
				+ " tiene asocidada el código de factura " + codigoFactura);
		
		biConsumerLambda.accept(idCliente, codigoFactura);
	}
	
	// Lambda que hace uso de una Function cuya interfaz hemos definido. Devuelve un doble aleatorio
	public static void functionLambdaDefiningInterface(int codigoEntrada) {
		InterfaceFunction functionLambda = (codigoProducto) ->  Math.random();
		
		System.out.println("Precio del producto con código " + codigoEntrada + ": " + functionLambda.getPrice(codigoEntrada));
	}
	
	// Lambda que hace uso de la interfaz ya definida Function. Devuelve un entero aleatorio entr 0 y 100
	public static void functionLambdaUsingExistentInterface(String nombreUsuario) {
		Function<String, Integer> functionLambda = (username) -> ThreadLocalRandom.current().nextInt(0, 99 + 1);
		
		System.out.println("Puntod acumulados por el usuario " + nombreUsuario + ": " + functionLambda.apply(nombreUsuario));
		
	}
	
	// Lambda haciendo uso de una biFunction con la interfaz definida por nosotros mismos.
	public static void biFunctionLambdaDefiningInterface(Double number1, Double number2) {
		InterfaceBiFunction biFunctionLambda = (num1,num2) -> num1*num2;
		
		System.out.println("El producto de " + number1 + " y " + number2 + " es " + 
				biFunctionLambda.getProduct(number1, number2));
	}
	
	// Lambda utilizando la interfaze biFunction
	public static void biFunctionLambdaUsingExistentnterface(Double number1, Double number2) {
		InterfaceBiFunction biFunctionLambda = (num1,num2) -> num1*num2;
		
		System.out.println("El producto de " + number1 + " y " + number2 + " es " + 
				biFunctionLambda.getProduct(number1, number2));
	}
	
	// Lambda haciendo uso de Predicate, definiendo nuestra propia interfaz
	public static void predicateLambdaDefiningInterface(int numeroEntrada) {
		InterfacePredicate predicateLambda = (num) -> num>=10;
		
		System.out.println("¿Es " + numeroEntrada +  " mayor que diez?: " + 
				predicateLambda.numberBiggerThan10(numeroEntrada)) ;
	}
	
	// Lambda utilizando la interfaz Predicate
	public static void predicateLambdaUsingExistentInterface(String entrada) {
		Predicate<String> predicateLambda = (word) -> word.length() >= 10;
		
		System.out.println("¿Tiene la palabra '" + entrada +  "' más de diez caracteres?: " + 
				predicateLambda.test(entrada)) ;
	}
	
	// Lambda haciendo uso de una interfaz BiPredicate que hemos definido nostoros mismos
	public static void biPredicateLambdaDfiningInterface(int number1, int number2) {
	InterfaceBiPredicate biPredicateLambda = (num1, num2) -> num1>num2;
	
	System.out.println("¿Es " + number1 + " mayor que " + number2 + "?: " + 
			biPredicateLambda.num1BiggetThanNum2(number1, number2));
	
	}
	
	// Lambda haciendo uso de la interfaz ya definida BiPredicate
	public static void biPredicateUsingExistentInterface(String palabra1, String palabra2) {
		BiPredicate<String,String> biPredicateLambda = (str1, str2) -> str1.length() <= str2.length();
		
		System.out.println("¿Tiene " + palabra1 + " un número menor o igual de caracteres que " + 
		palabra2 + "?: " + biPredicateLambda.test(palabra1, palabra2));
	}
	
}
