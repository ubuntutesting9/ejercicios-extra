package com.curso.lambdas;

public interface InterfaceFunction {

	public abstract Double getPrice(int codeProduct);
	
}
