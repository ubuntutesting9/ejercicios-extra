package com.curso.lambdas;

@FunctionalInterface
public interface InterfaceConsumeValue {

	public abstract void consumeValue(int numero);
	
}
