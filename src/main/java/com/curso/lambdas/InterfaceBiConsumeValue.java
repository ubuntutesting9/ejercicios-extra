package com.curso.lambdas;

public interface InterfaceBiConsumeValue {

	public abstract void consumeTwoValues(int age, String name);
	
}
