package com.curso.lambdas;

public interface InterfacePredicate {

	public abstract boolean numberBiggerThan10(int num);
	
}
